<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return redirect('/');
});
Route::get('/homeprop', function () {
    return view('homeprop');
});
Route::get('/homeprad', function () {
    return view('homeprad');
});
Route::get('/homealuno', function () {
    return view('homealuno');
});
Route::get('/homeauditoria', function () {
    return view('homeauditoria');
});

Route::get('/homedocente', function () {
    return view('homedocente');
});
Route::get('/hometecnico', function () {
    return view('hometecnico');
});
Route::get('/', function () {
   return view('login');
});
Route::get('/homeproplan', function () {
    return view('homeproplan');
});
//Route::get('/requerimentodiaria', function () {
    //return view('tela_requerimento_diaria_docente');
//});
//Route::get('/requerimentopassagem', function () {
    //return view('tela_requerimento_passagem_docente');
//});

Route::get('/homealuno', function () {
    return view('homealuno');
});


//Rotas para docente
Route::get('/docentepassagem','RequerimentoDocController@passagem');
Route::get('/docentediaria','RequerimentoDocController@diaria');
Route::get('/docentehistorico','RequerimentoDocController@read');
Route::post('/solicitacaodiaria','RequerimentoDocController@requerimentodiaria');
Route::post('/solicitacaopassagem','RequerimentoDocController@requerimentopassagem');
Route::get('voltarDocente','RequerimentoDocController@back');
Route::get('prestacaoformulariodocente/{id}','RequerimentoDocController@alterarStatus');
Route::post('prestacaocontadocente/{id}','RequerimentoDocController@prestarConta');

//Rotas para tecnico
Route::get('/tecnicopassagem', 'RequerimentoTecController@passagem');
Route::get('/tecnicodiaria', 'RequerimentoTecController@diaria');
Route::get('/tecnicohistorico', 'RequerimentoTecController@read');
Route::post('/solicitacaodiariatecnico','RequerimentoTecController@requerimentodiaria');
Route::post('/solicitacaopassagemtecnico','RequerimentoTecController@requerimentopassagem');
Route::get('voltarTecnico','RequerimentoTecController@back');
Route::get('prestacaoformulariotecnico/{id}','RequerimentoTecController@alterarStatus');
Route::post('prestacaocontatecnico/{id}','RequerimentoTecController@prestarConta');

//Rotas para aluno
Route::get('/auxiliofinanceiro','RequerimentoAluController@ajuda_financeira');
Route::post('/solicitarauxilio','RequerimentoAluController@requerimentofinanceiro');
Route::get('/alunohistorico','RequerimentoAluController@read');
Route::get('voltarAluno','RequerimentoAluController@back');
Route::get('/testealuno','RequerimentoAluController@testeRequerimento');
Route::get('prestacaoformularioaluno/{id}','RequerimentoAluController@alterarStatus');
Route::post('prestacaocontaaluno/{id}','RequerimentoAluController@prestarConta');


//Rotas da PROP
Route::get('/solicitacoes','PropController@solicitacoes');
Route::get('solicitacao/{id}','PropController@visualizar');
Route::get('solicitacaoAluno/{id}','PropController@visualizarAluno');
Route::get('deferirDocente/{id}','PropController@deferirDocente');
Route::get('deferirAluno/{id}','PropController@deferirAluno');
Route::get('deferirTecnico/{id}','PropController@deferirTecnico');
Route::get('solicitacaoTecnico/{id}','PropController@visualizarTecnico');
Route::get('indeferirConfirmacao/{id}','PropController@indeferirConfirmacao');
Route::post('indeferir/{id}','PropController@indeferir');
Route::get('voltarProp','PropController@back');


//Rotas da PRAD
Route::get('pradsolicitacoes','PradController@solicitacoes');
Route::get('solicitacaoDocentePrad/{id}','PradController@visualizarDocente');
Route::get('solicitacaoAlunoPrad/{id}','PradController@visualizarAluno');
Route::get('deferirDocentePrad/{id}','PradController@deferirDocente');
Route::get('deferirAlunoPrad/{id}','PradController@deferirAluno');
Route::get('deferirTecnicoPrad/{id}','PradController@deferirTecnico');
Route::get('solicitacaoTecnicoPrad/{id}','PradController@visualizarTecnico');
Route::get('indeferirConfirmacaoPrad/{id}','PradController@indeferirConfirmacao');
Route::post('indeferirPrad/{id}','PradController@indeferir');
Route::get('voltarPrad','PradController@back');


//Rotas da PROPLAN
Route::get('proplansolicitacoes','ProplanController@solicitacoes');
Route::get('solicitacaoDocenteProplan/{id}','ProplanController@visualizarDocente');
Route::get('solicitacaoAlunoProplan/{id}','ProplanController@visualizarAluno');
Route::get('deferirDocenteProplan/{id}','ProplanController@deferirDocente');
Route::get('deferirAlunoProplan/{id}','ProplanController@deferirAluno');
Route::get('deferirTecnicoProplan/{id}','ProplanController@deferirTecnico');
Route::get('solicitacaoTecnicoProplan/{id}','ProplanController@visualizarTecnico');
Route::get('indeferirConfirmacaoProplan/{id}','ProplanController@indeferirConfirmacao');
Route::post('indeferirProplan/{id}','ProplanController@indeferir');
Route::get('voltarProplan','ProplanController@back');

//Rotas da AUDITORIA
Route::get('auditoriasolicitacoes','AuditoriaController@solicitacoes');
Route::get('solicitacaoDocenteAuditoria/{id}','AuditoriaController@visualizarDocente');
Route::get('solicitacaoAlunoAuditoria/{id}','AuditoriaController@visualizarAluno');
Route::get('solicitacaoTecnicoAuditoria/{id}','AuditoriaController@visualizarTecnico');
Route::get('deferirDocenteAuditoria/{id}','AuditoriaController@deferirDocente');
Route::get('deferirAlunoAuditoria/{id}','AuditoriaController@deferirAluno');
Route::get('deferirTecnicoAuditoria/{id}','AuditoriaController@deferirTecnico');
Route::get('indeferirConfirmacaoAuditoria/{id}','AuditoriaController@indeferirConfirmacao');
Route::post('indeferirAuditoria/{id}','AuditoriaController@indeferir');
Route::get('voltarAuditoria','AuditoriaController@back');

//Rota usuario
Route::post('/','UsuarioController@logout');
Route::get('/destroi','RequerimentoTecController@destroi');
Route::get('/usuario', 'UsuarioController@index');
Route::get('/teste', 'UsuarioController@teste');
Route::get('/dest', 'UsuarioController@destro');
Route::post('/home','UsuarioController@login');