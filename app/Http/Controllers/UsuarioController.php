<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;

class UsuarioController extends Controller
{
    public function index(){
	    $usuario = new Usuario();
	    $usuario->matricula = '1000002';
	    $usuario->senha = 'procrastinador';
	    $usuario->cargo = 'auditoria';
        $usuario->nome = 'Adquison Júnior';
        $usuario->email =  'krunkerJr@hotmail.com';
        $usuario->centro = 'Pirajá';
        $usuario->campus = 'Torquato Neto';
        $usuario->telefone = '3222-5100';
	    $usuario->save();
	    return redirect('login');
	}
	public function teste(){
		$usuarios = Usuario::All();
		return view('teste',compact('usuarios'));
	}
	public function destroy()
    {
        $usuario = Usuario::find('5ce697bac4b3c300064b1d02');
        $usuario->delete();
        return redirect('login');
    }
    public function login(Request $request){
    	$mat = $request->get('matricula');
    	$senha = $request->get('senha');
    	$usuarios = Usuario::where('matricula','=',$mat)->get();	  	
    	$st = isset($usuarios[0]) ? $usuarios[0] : false;
		if ($st){
			$usuario = $usuarios[0];
    		$matricula = $usuario->matricula;
            session_start();
            $_SESSION['matricula'] = $mat;
            $_SESSION['nome'] = $usuario->nome;
    		if($usuario->cargo == 'docente'){
                return view('homedocente');
    		}else if ($usuario->cargo == 'tecnico'){
    			return view('hometecnico');
            }else if ($usuario->cargo == 'auditoria'){
    			return view('homeauditoria');
    		}else if($usuario->cargo == 'prop'){
                    return view('homeprop');
            }else if($usuario->cargo == 'prad'){
                    return view('homeprad');
            }else if($usuario->cargo == 'proplan'){
                    return view('homeproplan');
            }else{
                return view('homealuno');
            }
		}else{
			return redirect('/');
		}
    }
    public function logout(){
        session_start();
        session_unset();
        session_destroy();
        return redirect('/');
    }
}
