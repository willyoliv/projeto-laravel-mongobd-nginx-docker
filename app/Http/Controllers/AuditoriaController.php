<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequerimentoD;
use App\RequerimentoA;
use App\RequerimentoT;

class AuditoriaController extends Controller
{
    public function solicitacoes(){
    	$status = "AUDITORIA";
        $requerimentos_docente = RequerimentoD::where('status','=',$status)->get();
        $requerimentos_tecnico = RequerimentoT::where('status','=',$status)->get();
        $requerimentos_aluno = RequerimentoA::where('status','=',$status)->get();
        return view('tela_solicitacoes_auditoria', compact('requerimentos_docente','requerimentos_tecnico','requerimentos_aluno'));
    }
    public function visualizarDocente($id){
        $requerimento_docente = RequerimentoD::find($id);
        return view('tela_formulario_auditoria_docente',compact('requerimento_docente'));
    }
    public function visualizarAluno($id){
        $requerimento_aluno = RequerimentoA::find($id);
        $matricula_professor = $requerimento_aluno->matricula_professor;
        $requerimento_docentes = RequerimentoD::where('matricula','=',$matricula_professor)->get();
        $requerimento_docente = $requerimento_docentes[0];
        return view('tela_formulario_auditoria_aluno',compact('requerimento_docente','requerimento_aluno'));
    }
    public function visualizarTecnico($id){
        $requerimento_tecnico = RequerimentoT::find($id);
        return view('tela_formulario_auditoria_tecnico',compact('requerimento_tecnico'));
    }
    public function deferirDocente($id){
        $status = "DEFERIDO";
        $requerimento_docente = RequerimentoD::find($id);
        $std = isset($requerimento_docente) ? $requerimento_docente : false;
        if($std){
            $requerimento_docente->status = $status;
            $requerimento_docente->save();
        }
        return redirect('homeauditoria');
    }
    public function deferirAluno($id){
        $status = "DEFERIDO";
        $requerimento_aluno = RequerimentoA::find($id);
        $std = isset($requerimento_aluno) ? $requerimento_aluno : false;
        if($std){
            $requerimento_aluno->status = $status;
            $requerimento_aluno->save();
        }
        return redirect('homeauditoria');
    }
    public function deferirTecnico($id){
        $status = "DEFERIDO";
        $requerimento_tecnico = RequerimentoT::find($id);
        $std = isset($requerimento_tecnico) ? $requerimento_tecnico : false;
        if($std){
            $requerimento_tecnico->status = $status;
            $requerimento_tecnico->save();
        }
        return redirect('homeauditoria');
    }
    public function indeferirConfirmacao($id){
        return view('tela_indeferir_auditoria',compact('id'));
    }

    public function indeferir(Request $request, $id){
        $status = "INDEFERIDO";
    	$requerimento_aluno = requerimentoA::find($id);
    	$requerimento_tecnico = RequerimentoT::find($id);
    	$requerimento_docente = RequerimentoD::find($id);
    	$sta = isset($requerimento_aluno) ? $requerimento_aluno : false;
    	$stt = isset($requerimento_tecnico) ? $requerimento_tecnico : false;
    	$std = isset($requerimento_docente) ? $requerimento_docente : false;
    	if($sta){
    		$requerimento_aluno->observacao = $request->get('observacao');
    		$requerimento_aluno->status = $status;
    		$requerimento_aluno->save();
    	}else if($stt){
    		$requerimento_tecnico->observacao = $request->get('observacao');
    		$requerimento_tecnico->status = $status;
    		$requerimento_tecnico->save();
    	}else{
    		$requerimento_docente->observacao = $request->get('observacao');
    		$requerimento_docente->status = $status;
    		$requerimento_docente->save();
    	}
        return redirect('homeauditoria');
    }
    public function back(){
        return redirect('homeauditoria');
    }
}
