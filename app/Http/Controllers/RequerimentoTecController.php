<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequerimentoT;
use App\Usuario;
use Carbon\Carbon;


class RequerimentoTecController extends Controller
{
    //
    public function passagem()
    {
        return view('tela_requerimento_passagem_tecnico');
    }

    public function diaria()
    {
        return view('tela_requerimento_diaria_tecnico');
    }
    public function historico()
    {
        return view('tela_historico_tecnico');
    }

    public function requerimentopassagem(Request $request)
    {   
        $RequerimentoT = new RequerimentoT();
        $dados = $request->all();
        $this->validate($request,$RequerimentoT->rules,$RequerimentoT->messages);
        // $this->validate($request,$RequerimentoD->rules,$RequerimentoD->messages);
        $insert = $RequerimentoT->create($dados);
        if($insert){
            return redirect('hometecnico');
        }else{
            return redirect('tecnicopassagem');
        }
        /*session_start();
        $RequerimentoT = new RequerimentoT();
        $RequerimentoT->matricula = $_SESSION['matricula'];
        $mat = $_SESSION['matricula'];
        $usuarios = Usuario::where('matricula','=',$mat)->get();
        $RequerimentoT->nome = $usuarios[0]->nome;       
        $RequerimentoT->email = $request->get('email');
        $RequerimentoT->campus = $request->get('campus');
        $RequerimentoT->centro = $request->get('centro');
        $RequerimentoT->conta = $request->get('conta');
        $RequerimentoT->status = "PROP";
        $RequerimentoT->data_ida = $request->get('data_ida');
        $RequerimentoT->data_volta = $request->get('data_volta');
        $RequerimentoT->agencia = $request->get('agencia');
        $RequerimentoT->abrangencia_evento = $request->get('abrangencia_evento');
        $RequerimentoT->auxilio_anterior = $request->get('auxilio_anterior');       
        $RequerimentoT->banco = $request->get('banco');
        $RequerimentoT->evento = $request->get('evento');
        $RequerimentoT->comprovante_trabalho = $request->get('comprovante_trabalho');
        $RequerimentoT->trabalho = $request->get('trabalho');
        $RequerimentoT->ano = $request->get('ano');
        $RequerimentoT->descricao = $request->get('descricao');
        $RequerimentoT->contracheque = $request->get('contracheque');
        $RequerimentoT->tipo = "passagem"; 
        $RequerimentoT->data_solicitacao = Carbon::now()->toDateTimeString();    
        $RequerimentoT->save();
        return view('hometecnico');*/
    }

    public function requerimentodiaria(Request $request)
    {   
        $RequerimentoT = new RequerimentoT();
        $dados = $request->all();
        $this->validate($request,$RequerimentoT->rules,$RequerimentoT->messages);
        // $this->validate($request,$RequerimentoD->rules,$RequerimentoD->messages);
        $insert = $RequerimentoT->create($dados);
        if($insert){
            return redirect('hometecnico');
        }else{
            return redirect('tecnicopassagem');
        }
        /*session_start();
        $RequerimentoT = new RequerimentoT();
        $RequerimentoT->matricula = $_SESSION['matricula'];
        $mat = $_SESSION['matricula'];
        $usuarios = Usuario::where('matricula','=',$mat)->get();
        $RequerimentoT->nome = $usuarios[0]->nome;       
        $RequerimentoT->email = $request->get('email');
        $RequerimentoT->campus = $request->get('campus');
        $RequerimentoT->centro = $request->get('centro');
        $RequerimentoT->conta = $request->get('conta');
        $RequerimentoT->status = "PROP";
        $RequerimentoT->data_ida = $request->get('data_ida');
        $RequerimentoT->data_volta = $request->get('data_volta');
        $RequerimentoT->agencia = $request->get('agencia');
        $RequerimentoT->abrangencia_evento = $request->get('abrangencia_evento');
        $RequerimentoT->auxilio_anterior = $request->get('auxilio_anterior');       
        $RequerimentoT->banco = $request->get('banco');
        $RequerimentoT->evento = $request->get('evento');
        $RequerimentoT->comprovante_trabalho = $request->get('comprovante_trabalho');
        $RequerimentoT->trabalho = $request->get('trabalho');
        $RequerimentoT->ano = $request->get('ano');
        $RequerimentoT->descricao = $request->get('descricao');
        $RequerimentoT->contracheque = $request->get('contracheque');
        $RequerimentoT->tipo = "diaria";   
        $RequerimentoT->data_solicitacao = Carbon::now()->toDateTimeString();  
        $RequerimentoT->save();
        return view('hometecnico');*/
    }

    public function read(){
        
        session_start();
        $matricula = $_SESSION['matricula'];
        $requerimento_finalizados = RequerimentoT::where('matricula','=',$matricula)->where('status','=','DEFERIDO')->get();
        $requerimento_indeferidos = RequerimentoT::where('matricula','=',$matricula)->where('status','=','INDEFERIDO')->get();
        $requerimento_prestacao = RequerimentoT::where('matricula','=',$matricula)->where('status','=','PRESTARCONTA')->get();
        $requerimento_andamento = RequerimentoT::where('matricula','=',$matricula)->where('status','!=','PRESTARCONTA')->where('status','!=','DEFERIDO')->where('status','!=','INDEFERIDO')->get();
        return view('tela_historico_tecnico', compact('requerimento_indeferidos','requerimento_prestacao','requerimento_finalizados','requerimento_andamento'));
    }
    public function back(){
        return redirect('hometecnico');
    }

    public function alterarStatus($id){
        $requerimento_tecnico = RequerimentoT::find($id);
        return view('tela_pdf_prestacao_tecnico',compact('requerimento_tecnico'));
    }
    public function prestarConta(Request $request,$id){
        $status = "AUDITORIA";
        $requerimento_tecnico = RequerimentoT::find($id);
        $std = isset($requerimento_tecnico) ? $requerimento_tecnico : false;
        if($std){
            $requerimento_tecnico->status = $status;
            $requerimento_tecnico->comprovante_evento = $request->get('comprovante_evento');
            $requerimento_tecnico->save();
        }
        return redirect('hometecnico');
    }


    //LIXO COISAS PARA TESTE
    public function destroy($id)
    {
        $RequerimentoT = RequerimentoT::find($id);
        $RequerimentoT->delete();
        return redirect('hometecnico');
    }

    public function destroi(){
        $requerimento = RequerimentoT::truncate();
        return redirect('/');
    }
    //FIM TESTE

}
