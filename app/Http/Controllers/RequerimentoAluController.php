<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequerimentoA;
use App\RequerimentoD;
use App\Usuario;

class RequerimentoAluController extends Controller
{   
    public function ajuda_financeira() {
        return view('tela_requerimento_auxilio_aluno');
    }
	public function requerimentofinanceiro(Request $request)
    {
        $matricula_professor = $request->get('matricula_professor');
        $nome_professor = $request->get('nome_professor');
        $dados_professor = RequerimentoD::where('matricula','=',$matricula_professor)->where('nome','=',$nome_professor)->get();
        $sta = isset($dados_professor[0]) ? $dados_professor[0] : false;
        if($sta){
            $RequerimentoA = new RequerimentoA();
            $dados = $request->all();
            $this->validate($request,$RequerimentoA->rules,$RequerimentoA->messages);
            $insert = $RequerimentoA->create($dados);
            if($insert){
                return redirect('homealuno');
            }else{
                return redirect('auxiliofinanceiro');
            }   
       }else{
            $erro = 'Professor não tem pedido';
           // return view('tela_requerimento_auxilio_aluno',compact('erro'));
            //return back()->with($erro);
            //return redirect()->back()->with('success', ['Professor não tem pedido']);
            session()->flash('msg', 'Professor não tem pedido');
            return redirect()->back();
       }
         
        
    }
    public function read(){
        session_start();
        $matricula = $_SESSION['matricula'];
        $requerimento_finalizados = RequerimentoA::where('matricula','=',$matricula)->where('status','=','DEFERIDO')->get();
        $requerimento_indeferidos = RequerimentoA::where('matricula','=',$matricula)->where('status','=','INDEFERIDO')->get();
        $requerimento_prestacao = RequerimentoA::where('matricula','=',$matricula)->where('status','=','PRESTARCONTA')->get();
        $requerimento_andamento = RequerimentoA::where('matricula','=',$matricula)->where('status','!=','PRESTARCONTA')->where('status','!=','DEFERIDO')->where('status','!=','INDEFERIDO')->get();
        return view('tela_historico_aluno', compact('requerimento_indeferidos','requerimento_prestacao','requerimento_finalizados','requerimento_andamento'));
    }
    public function back(){
        return redirect('homealuno');
    }
    public function alterarStatus($id){
        $requerimento_aluno = RequerimentoA::find($id);
        return view('tela_pdf_prestacao_aluno',compact('requerimento_aluno'));
    }
    public function prestarConta(Request $request,$id){
        $status = "AUDITORIA";
        $requerimento_aluno = RequerimentoA::find($id);
        $std = isset($requerimento_aluno) ? $requerimento_aluno : false;
        if($std){
            $requerimento_aluno->status = $status;
            $requerimento_aluno->comprovante_evento = $request->get('comprovante_evento');
            $requerimento_aluno->save();
        }
        return redirect('homealuno');
    }
    

    //LIXO COISAS PARA TESTE
    public function destroy($id)
    {
        $RequerimentoD = RequerimentoD::find($id);
        $RequerimentoD->delete();
        return redirect('#')->with('success','Requerimento do aluno foi deletado com sucesso');
    }
    public function destroi(){
        $requerimento = RequerimentoA::truncate();
        return redirect('/');
    }
    public function testeRequerimento(){
        echo "<pre>";
        echo RequerimentoA::all();
        echo "</pre>";
    }
    //FIM TESTE

}
