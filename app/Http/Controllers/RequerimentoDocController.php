<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequerimentoD;
use App\Usuario;
use Carbon\Carbon;

class RequerimentoDocController extends Controller
{
    //
    public function passagem() {
        return view('tela_requerimento_passagem_docente');
    }

    public function diaria(){
        return view('tela_requerimento_diaria_docente');
    }

    public function historico(){
        return view('tela_historico_docente');
    }

    public function requerimentodiaria(Request $request)
    {   
        $RequerimentoD = new RequerimentoD();
        $dados = $request->all();
        $this->validate($request,$RequerimentoD->rules,$RequerimentoD->messages);
        // $this->validate($request,$RequerimentoD->rules,$RequerimentoD->messages);
        $insert = $RequerimentoD->create($dados);
        if($insert){
            return redirect('homedocente');
        }else{
            return redirect('docentediaria');
        }
        
       /* $RequerimentoD = new RequerimentoD();
        $RequerimentoD->matricula = $_SESSION['matricula'];
        $mat = $_SESSION['matricula'];
        $usuarios = Usuario::where('matricula','=',$mat)->get();
        $RequerimentoD->nome = $usuarios[0]->nome;
        $RequerimentoD->data_ida = $request->get('data_ida');
        $RequerimentoD->data_volta = $request->get('data_volta');
        $RequerimentoD->abrangencia_evento = $request->get('abrangencia_evento');
        $RequerimentoD->auxilio_anterior = $request->get('auxilio_anterior');  
        $RequerimentoD->email = $request->get('email');
        $RequerimentoD->campus = $request->get('campus');
        $RequerimentoD->centro = $request->get('centro');
        $RequerimentoD->conta = $request->get('conta');
        $RequerimentoD->status = "PROP";
        $RequerimentoD->ano = $request->get('ano');
        $RequerimentoD->agencia = $request->get('agencia');
        $RequerimentoD->banco = $request->get('banco');
        $RequerimentoD->evento = $request->get('evento');
        $RequerimentoD->curriculo = $request->get('curriculo');
        $RequerimentoD->comprovante_trabalho = $request->get('comprovante_trabalho');
        $RequerimentoD->trabalho = $request->get('trabalho');
        $RequerimentoD->contracheque = $request->get('contracheque');
        $RequerimentoD->doutorado = 30*$request->get('doutorado');
        $RequerimentoD->mestrado = 15*$request->get('mestrado');
        $RequerimentoD->bolsista_produtividade = 10*$request->get('bolsista_produtividade');
        $RequerimentoD->qualis_A1 = 10*$request->get('qualis_A1');
        $RequerimentoD->qualis_A2 = 8*$request->get('qualis_A2');
        $RequerimentoD->qualis_B1 = 6*$request->get('qualis_B1');
        $RequerimentoD->qualis_B2 = 4*$request->get('qualis_B2');
        $RequerimentoD->qualis_B3 = 3*$request->get('qualis_B3');
        $RequerimentoD->qualis_B4_B5 = 2*$request->get('qualis_B4_B5');
        $RequerimentoD->qualis_C = 1*$request->get('qualis_C');
        $RequerimentoD->artigos_nao_qualis = 0.5*$request->get('artigos_nao_qualis');
        $RequerimentoD->eventos_internacionais = 2*$request->get('eventos_internacionais');
        $RequerimentoD->eventos_nacionais = 1*$request->get('eventos_nacionais');
        $RequerimentoD->patente_registrada = 10*$request->get('patente_registrada');
        $RequerimentoD->software_registrado = 10*$request->get('software_registrado');
        $RequerimentoD->livro_com_conselho = 10*$request->get('livro_com_conselho');
        $RequerimentoD->livro_sem_conselho = 5*$request->get('livro_sem_conselho');
        $RequerimentoD->livro_organizado = 5*$request->get('livro_organizado');
        $RequerimentoD->capitulo_de_livro = 3*$request->get('capitulo_de_livro');
        $RequerimentoD->projeto_externo_coordenador = 10*$request->get('projeto_externo_coordenador');
        $RequerimentoD->projeto_externo_colaborador = 5*$request->get('projeto_externo_colaborador');
        $RequerimentoD->projeto_interno_coordenador = 5*$request->get('projeto_interno_coordenador');
        $RequerimentoD->projeto_interno_colaborador = 3*$request->get('projeto_interno_colaborador');
        $RequerimentoD->orientacao_doutorado = 10*$request->get('orientacao_doutorado');
        $RequerimentoD->orientacao_mestrado = 8*$request->get('orientacao_mestrado');
        $RequerimentoD->orientacao_iniciacao_cientifica = 4*$request->get('orientacao_iniciacao_cientifica');
        $RequerimentoD->orientacao_TCC = 2*$request->get('orientacao_TCC');
        $RequerimentoD->orientacao_iniciacao_cientifica_junior = 0.5*$request->get('orientacao_iniciacao_cientifica_junior');
        $RequerimentoD->orientacao_monografia = 2*$request->get('orientacao_monografia');
        $RequerimentoD->co_orientacao_doutorado = 6*$request->get('co_orientacao_doutorado');
        $RequerimentoD->bancas_de_defesa_doutorado = 6*$request->get('bancas_de_defesa_doutorado');
        $RequerimentoD->bancas_de_defesa_qualificacao_doutorado = 3*$request->get('bancas_de_defesa_qualificacao_doutorado');
        $RequerimentoD->bancas_de_defesa_qualificacao_mestrado = 3*$request->get('bancas_de_defesa_qualificacao_mestrado');
        $RequerimentoD->bancas_de_defesa_mestrado = 2*$request->get('bancas_de_defesa_mestrado');
        $RequerimentoD->bancas_de_defesa_TCC = 1*$request->get('bancas_de_defesa_TCC');
        $RequerimentoD->membro_comite_etica = 6*$request->get('membro_comite_etica');
        $RequerimentoD->tipo = "diaria";
        $RequerimentoD->data_solicitacao = Carbon::now()->toDateTimeString();    
        $RequerimentoD->save();
        return view('homedocente');*/
    }
    public function requerimentopassagem(Request $request)
    {   
        $RequerimentoD = new RequerimentoD();
        $dados = $request->all();
        $this->validate($request,$RequerimentoD->rules,$RequerimentoD->messages);
        $insert = $RequerimentoD->create($dados);
        if($insert){
            return redirect('homedocente');
        }else{
            return redirect('docentepassagem');
        }
        /*session_start();
        $RequerimentoD=new RequerimentoD();
        $RequerimentoD->matricula = $_SESSION['matricula'];
        $mat = $_SESSION['matricula'];
        $usuarios = Usuario::where('matricula','=',$mat)->get();
        $RequerimentoD->nome = $usuarios[0]->nome;
        $RequerimentoD->data_ida = $request->get('data_ida');
        $RequerimentoD->data_volta = $request->get('data_volta');
        $RequerimentoD->abrangencia_evento = $request->get('abrangencia_evento');
        $RequerimentoD->auxilio_anterior = $request->get('auxilio_anterior');
        $RequerimentoD->email = $request->get('email');
        $RequerimentoD->campus = $request->get('campus');
        $RequerimentoD->centro = $request->get('centro');
        $RequerimentoD->conta = $request->get('conta');
        $RequerimentoD->ano = $request->get('ano');
        $RequerimentoD->status = "PROP";
        $RequerimentoD->agencia = $request->get('agencia');
        $RequerimentoD->banco = $request->get('banco');
        $RequerimentoD->evento = $request->get('evento');
        $RequerimentoD->curriculo = $request->get('curriculo');
        $RequerimentoD->comprovante_trabalho = $request->get('comprovante_trabalho');
        $RequerimentoD->trabalho = $request->get('trabalho');
        $RequerimentoD->contracheque = $request->get('contracheque');
        $RequerimentoD->doutorado = 30*$request->get('doutorado');
        $RequerimentoD->mestrado = 15*$request->get('mestrado');
        $RequerimentoD->bolsista_produtividade = 10*$request->get('bolsista_produtividade');
        $RequerimentoD->qualis_A1 = 10*$request->get('qualis_A1');
        $RequerimentoD->qualis_A2 = 8*$request->get('qualis_A2');
        $RequerimentoD->qualis_B1 = 6*$request->get('qualis_B1');
        $RequerimentoD->qualis_B2 = 4*$request->get('qualis_B2');
        $RequerimentoD->qualis_B3 = 3*$request->get('qualis_B3');
        $RequerimentoD->qualis_B4_B5 = 2*$request->get('qualis_B4_B5');
        $RequerimentoD->qualis_C = 1*$request->get('qualis_C');
        $RequerimentoD->artigos_nao_qualis = 0.5*$request->get('artigos_nao_qualis');
        $RequerimentoD->eventos_internacionais = 2*$request->get('eventos_internacionais');
        $RequerimentoD->eventos_nacionais = 1*$request->get('eventos_nacionais');
        $RequerimentoD->patente_registrada = 10*$request->get('patente_registrada');
        $RequerimentoD->software_registrado = 10*$request->get('software_registrado');
        $RequerimentoD->livro_com_conselho = 10*$request->get('livro_com_conselho');
        $RequerimentoD->livro_sem_conselho = 5*$request->get('livro_sem_conselho');
        $RequerimentoD->livro_organizado = 5*$request->get('livro_organizado');
        $RequerimentoD->capitulo_de_livro = 3*$request->get('capitulo_de_livro');
        $RequerimentoD->projeto_externo_coordenador = 10*$request->get('projeto_externo_coordenador');
        $RequerimentoD->projeto_externo_colaborador = 5*$request->get('projeto_externo_colaborador');
        $RequerimentoD->projeto_interno_coordenador = 5*$request->get('projeto_interno_coordenador');
        $RequerimentoD->projeto_interno_colaborador = 3*$request->get('projeto_interno_colaborador');
        $RequerimentoD->orientacao_doutorado = 10*$request->get('orientacao_doutorado');
        $RequerimentoD->orientacao_mestrado = 8*$request->get('orientacao_mestrado');
        $RequerimentoD->orientacao_iniciacao_cientifica = 4*$request->get('orientacao_iniciacao_cientifica');
        $RequerimentoD->orientacao_TCC = 2*$request->get('orientacao_TCC');
        $RequerimentoD->orientacao_iniciacao_cientifica_junior = 0.5*$request->get('orientacao_iniciacao_cientifica_junior');
        $RequerimentoD->orientacao_monografia = 2*$request->get('orientacao_monografia');
        $RequerimentoD->co_orientacao_doutorado = 6*$request->get('co_orientacao_doutorado');
        $RequerimentoD->bancas_de_defesa_doutorado = 6*$request->get('bancas_de_defesa_doutorado');
        $RequerimentoD->bancas_de_defesa_qualificacao_doutorado = 3*$request->get('bancas_de_defesa_qualificacao_doutorado');
        $RequerimentoD->bancas_de_defesa_mestrado = 2*$request->get('bancas_de_defesa_mestrado');
        $RequerimentoD->bancas_de_defesa_qualificacao_mestrado = 3*$request->get('bancas_de_defesa_qualificacao_mestrado');
        $RequerimentoD->bancas_de_defesa_TCC = 1*$request->get('bancas_de_defesa_TCC');
        $RequerimentoD->membro_comite_etica = 6*$request->get('membro_comite_etica');
        $RequerimentoD->tipo = "passagem"; 
        $RequerimentoD->data_solicitacao = Carbon::now()->toDateTimeString();    
        $RequerimentoD->save();
        return view('homedocente');*/
    }


    public function read(){
        session_start();
        $matricula = $_SESSION['matricula'];
        $requerimento_finalizados = RequerimentoD::where('matricula','=',$matricula)->where('status','=','DEFERIDO')->get();
        $requerimento_indeferidos = RequerimentoD::where('matricula','=',$matricula)->where('status','=','INDEFERIDO')->get();
        $requerimento_prestacao = RequerimentoD::where('matricula','=',$matricula)->where('status','=','PRESTARCONTA')->get();
        $requerimento_andamento = RequerimentoD::where('matricula','=',$matricula)->where('status','!=','PRESTARCONTA')->where('status','!=','DEFERIDO')->where('status','!=','INDEFERIDO')->get();
        return view('tela_historico_docente', compact('requerimento_indeferidos','requerimento_prestacao','requerimento_finalizados','requerimento_andamento'));
    }
    public function back(){
        return redirect('homedocente');
    }

    public function alterarStatus($id){
        $requerimento_docente = RequerimentoD::find($id);
        return view('tela_pdf_prestacao_docente',compact('requerimento_docente'));
    }
    public function prestarConta(Request $request,$id){
        $status = "AUDITORIA";
        $requerimento_docente = RequerimentoD::find($id);
        $std = isset($requerimento_docente) ? $requerimento_docente : false;
        if($std){
            $requerimento_docente->status = $status;
            $requerimento_docente->comprovante_evento = $request->get('comprovante_evento');
            $requerimento_docente->save();
        }
        return redirect('homedocente');
    }


    //LIXO COISAS PARA TESTE
    public function d()
    {
        $usuarios = RequerimentoD::All();
        return view('apagar',compact('usuarios'));
    }
    public function destroy(){
        $requerimento = RequerimentoD::find('5d1e3a27cc5fec000634d403');
        $requerimento->delete();
        return redirect('login');
    }
    public function destroi(){
        $requerimento = RequerimentoD::truncate();
        return redirect('/');
    }
    //FIM TESTE

}
