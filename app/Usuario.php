<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Usuario extends Eloquent
{
	
    protected $connection = 'mongodb';
    protected $collection = 'Usuario';
	protected $fillable = ['nome','matricula','senha','cargo','email','centro','campus','telefone'];    
    
}