<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Formulário PROP</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <style>
      /* RESET */
      * {margin: 0; padding: 0; font-size: 100%; font-family: 'Open Sans', sans-serif; font-weight: normal;
      box-sizing: border-box;}
      img {
        max-width: : 100%
      }
      ul {
        list-style: none;
      }
      a {
        text-decoration: none;
      }
      h2 {
        font-size: 1.5em; 
        color: #333;
      }
      p {
        font-size: 1em; 
        color: #777;
      }
      h1 {
        color: #ffffff;
      }
      h3 {
        font-size: 1.5em; 
      }

      /* CABEÇALHO */
      .cabecalho {
        width: 100%; 
        float: left; 
        padding: 15px 4%; 
        background-color: #3db0f7;
      }
      .cabecalho form {
        width: 30%; 
        float: right;
      }
      .cabecalho button {
        width: 15%; 
        float: right; 
        padding: 15px 10px; 
        background-color: #0cae98; 
        color: #fff;
        cursor: pointer;
      }
      .logo a {
        width: 71px; 
        height: 119px;
        float: left; 
        background: url(../img/logo1.png) no-repeat;
        margin-left: 0px;
        margin-right: 30px;
      }
      .titulo_site {
        float: left;
        margin-top: 30px;
        margin-left: 30px;
        font-weight: bold;
        color: #ffffff;  
      }
      .form-inline label{
        color: #ffffff;
        margin-right: 10px; 
      }
      .nav-link{
        margin-left: 25px;
      }
      .h3{
        color: #00008B;
        font-family: 'Open Sans', sans-serif;
        text-align: right;
      }
      table {
        font-family: 'Open Sans', sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
      }

      tr:nth-child(even) {
        background-color: #dddddd;
      }
      button#indeferir{
        margin-left: 10px;
      }
    </style>
  </head>
  <body>
    <header class="cabecalho">
      <h1 class="logo">
      <a title="UESPI - SIG Auxílios Acadêmicos"></a>
    </h1>
    <h1 class="titulo_site"> UESPI - SIG Auxílios Acadêmicos </h1>    
    </header>
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <div class="collapse navbar-collapse" id="nav-content">   
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class = "nav-link" href = "{{url('solicitacoes')}}" ><font color=white>Solicitações de Auxílios</font></a>
          </li>
        </ul>
        <form class="form-inline" action ="{{url('')}}" method="post">
          {!! csrf_field() !!}
            <button class="btn btn-outline-success" type="submit">Logout</button>
          </form>
        </div>
    </nav><br>
    <div class="container">
    	<form method="post">
    		{!! csrf_field() !!}
        <table>
          <tr>
            <h3>Dados do Requerente:</h3>
          </tr>
          <tr>
            <td>Conta do Banco:</td>
            <td>{{$requerimento_docente->conta}}</td>
          </tr>
           <tr>
            <td>Agência:</td>
            <td>{{$requerimento_docente->agencia}}</td>
          </tr>
          <tr>
            <td>Nome do Banco:</td>
            <td>{{$requerimento_docente->banco}}</td>
          </tr>
          <tr>
            <td>Local do Evento:</td>
            <td>{{$requerimento_docente->evento}}</td>
          </tr>
          <tr>
            <td>Período do Evento(Ínicio/Término):</td>
            <td>{{ date( 'd/m/Y' , strtotime($requerimento_docente->data_ida))}} \ {{ date( 'd/m/Y' , strtotime($requerimento_docente->data_volta))}}</td>
          </tr>
          <tr>
            <td>Abrangência do Evento:</td>
            <td>{{$requerimento_docente->abrangencia_evento}}</td>
          </tr>
          <tr>
            <td>Título do Trabalho:</td>
            <td>{{$requerimento_docente->trabalho}}</td>
          </tr>
          <tr>
            <td>Recebeu auxílio nos anos anteriores?</td>
            <td>{{$requerimento_docente->auxilio_anterior}}</td>
          </tr>
          <tr>
            <td>Ano:</td>
            <td>{{$requerimento_docente->ano}}</td>
          </tr>
          <tr>
            <td>Descrição do Auxílio:</td>
            <td>{{$requerimento_docente->descricao}}</td>
          </tr>
          <tr>
            <td>Contra-Cheque:</td>
            <td>{{$requerimento_docente->contracheque}}</td>
          </tr>
          <tr>
            <td>Currículo-Lattes:</td>
            <td>{{$requerimento_docente->curriculo}}</td>
          </tr>
          <tr>
            <td>PDF do Trabalho:</td>
            <td>{{$requerimento_docente->comprovante_trabalho}}</td>
          </tr>
        </table>

        <h2>Formulário de Produção</h2>
        <table>          
          <tr>
           <h3>Área de Avaliação: </h3>
          </tr>
          <tr>
            <td>Discriminação</td>
            <td>Escore por item</td>
            <td>Quantidade</td>
        </tr>
            <tr>
              <td>Doutorado</td>
              <td>30</td>
              <td>{{$requerimento_docente->doutorado*30}}</td>
              
            </tr>
            <tr>
              <td>Mestrado</td>
              <td>15</td>
              <td>{{$requerimento_docente->mestrado*15}}</td>
              
            </tr>
            <tr>
              <td>Bolsista de Produtividade em Pesquisa do CNPq</td>
              <td>10</td>
              <td>{{$requerimento_docente->bolsista_produtividade*10}}</td>
              
            </tr>
            <tr>
              <td>Artigos publicados em periódicos científicos indexados:</td>
              <td> </td>
              <td> </td>
              
            </tr>
            <tr>
              <td>Qualis A1</td>
              <td>10</td>
              <td>{{$requerimento_docente->qualis_A1*10}}</td>
              
            </tr>
            <tr>
              <td>Qualis A2</td>
              <td>8</td>
              <td>{{$requerimento_docente->qualis_A2*8}}</td>
              
            </tr>
            <tr>
              <td>Qualis B1</td>
              <td>6</td>
              <td>{{$requerimento_docente->qualis_B1*6}}</td>
              
            </tr>
            <tr>
              <td>Qualis B2</td>
              <td>4</td>
              <td>{{$requerimento_docente->qualis_B2*4}}</td>
              
            </tr>
            <tr>
              <td>Qualis B3</td>
              <td>3</td>
              <td>{{$requerimento_docente->qualis_B3*3}}</td>
              
            </tr>
            <tr>
              <td>Qualis B4; B5</td>
              <td>2</td>
              <td>{{$requerimento_docente->qualis_B4_B5*2}}</td>
              
            </tr>
            <tr>
              <td>Qualis C</td>
              <td>1</td>
              <td>{{$requerimento_docente->qualis_C*1}}</td>
              
            </tr>
            <tr>
              <td>Artigos publicados em periódicos científicos NÃO 
              classificados pelo sistema QUALIS</td>
              <td>0.5</td>
              <td>{{$requerimento_docente->artigos_nao_qualis*0.5}}</td>
              
            </tr>
            <tr>
              <td>Trabalhos completos publicados em anais de congressos:</td>
              <td> </td>
              <td> </td>
              
            </tr>
            <tr>
              <td>Eventos Internacionais</td>
              <td>2</td>
              <td>{{$requerimento_docente->eventos_internacionais*2}}</td>
              
            </tr>
            <tr>
              <td>Eventos Nacionais e Regionais</td>
              <td>1</td>
              <td>{{$requerimento_docente->eventos_nacionais*1}}</td>
              
            </tr>
            <tr>
              <td>Patente Registrada (carta patente)</td>
              <td>10</td>
              <td>{{$requerimento_docente->patente_registrada*10}}</td>
              
            </tr>
            <tr>
              <td>Software Registrado</td>
              <td>10</td>
              <td>{{$requerimento_docente->software_registrado*10}}</td>
              
            </tr>
            <tr>
              <td>Autor/Co-autor de livros publicados com ISBN:</td>
              <td> </td>
              <td> </td>
              
            </tr>
            <tr>
              <td>Livro publicado por editora com conselho editorial, com no mínimo 60 páginas</td>
              <td>10</td>
              <td>{{$requerimento_docente->livro_com_conselho*10}}</td>
              
            </tr>
            <tr>
              <td>Livro publicado sem conselho editorial, com no mínimo 60 páginas</td>
              <td>5</td>
              <td>{{$requerimento_docente->livro_sem_conselho*5}}</td>
              
            </tr>
            <tr>
              <td>Livro Organizado</td>
              <td>5</td>
              <td>{{$requerimento_docente->livro_organizado*5}}</td>
              
            </tr>
            <tr>
              <td>Capítulo de livro publicado por editora com conselho editorial</td>
              <td>3</td>
              <td>{{$requerimento_docente->capitulo_de_livro*3}}</td>
              
            </tr>
            <tr>
              <td>Projeto de pesquisa com financiamento externo:</td>
              <td> </td>
              <td> </td>
              
            </tr>
            <tr>
              <td>Como coordenador</td>
              <td>10</td>
              <td>{{$requerimento_docente->projeto_externo_coordenador*10}}</td>
              
            </tr>
            <tr>
              <td>Como colaborador</td>
              <td>5</td>
              <td>{{$requerimento_docente->projeto_externo_colaborador*5}}</td>
              
            </tr>
            <tr>
              <td>Projeto de pesquisa com financiamento interno(Editais PIP):</td>
              <td> </td>
              <td> </td>
              
            </tr>
            <tr>
              <td>Como coordenador</td>
              <td>5</td>
              <td>{{$requerimento_docente->projeto_interno_coordenador*5}}</td>
              
            </tr>
            <tr>
              <td>Como colaborador</td>
              <td>3</td>
              <td>{{$requerimento_docente->projeto_interno_colaborador*3}}</td>
              
            </tr>
            <tr>
              <td>Orientação/Co-orientação(Concluída):</td>
              <td> </td>
              <td> </td>
              
            </tr>
            <tr>
              <td>Orientação de Doutorado</td>
              <td>10</td>
              <td>{{$requerimento_docente->orientacao_doutorado*10}}</td>
              
            </tr>
            <tr>
              <td>Orientação de Mestrado</td>
              <td>8</td>
              <td>{{$requerimento_docente->orientacao_mestrado*8}}</td>
              
            </tr>
            <tr>
              <td>Orientação de Iniciação Científica</td>
              <td>4</td>
              <td>{{$requerimento_docente->orientacao_iniciacao_cientifica*4}}</td>
              
            </tr>
            <tr>
              <td>Orientação de TCC de Graduação</td>
              <td>2</td>
              <td>{{$requerimento_docente->orientacao_TCC*2}}</td>
              
            </tr>
            <tr>
              <td>Orientação de Iniciação Científica Júnior</td>
              <td>0.5</td>
              <td>{{$requerimento_docente->orientacao_iniciacao_cientifica_junior*0.5}}</td>
              
            </tr>
            <tr>
              <td>Orientação de Monografia de Especialização</td>
              <td>2</td>
              <td>{{$requerimento_docente->orientacao_monografia*2}}</td>
              
            </tr>
            <tr>
              <td>Co-orientação de Doutorado</td>
              <td>6</td>
              <td>{{$requerimento_docente->co_orientacao_doutorado*6}}</td>
              
            </tr>
            <tr>
              <td>Participação em bancas de defesa:</td>
              <td> </td>
              <td> </td>
              
            </tr>
            <tr>
              <td>Doutorado</td>
              <td>6</td>
              <td>{{$requerimento_docente->bancas_de_defesa_doutorado*6}}</td>
              
            </tr>
            <tr>
              <td>Qualificação de Doutorado</td>
              <td>3</td>
              <td>{{$requerimento_docente->bancas_de_defesa_qualificacao_doutorado*3}}</td>
              
            </tr>
            <tr>
              <td>Mestrado</td>
              <td>4</td>
              <td>{{$requerimento_docente->bancas_de_defesa_mestrado*4}}</td>
              
            </tr>
            <tr>
              <td>Qualificação de Mestrado</td>
              <td>2</td>
              <td>{{$requerimento_docente->bancas_de_defesa_qualificacao_mestrado*2}}</td>
              
            </tr>
            <tr>
              <td>TCC/Monografia</td>
              <td>1</td>
              <td>{{$requerimento_docente->bancas_de_defesa_TCC*1}}</td>
              
            </tr>
            <tr>
              <td>Membro do Comitê de Ética em Pesquisa da UESPI (por ano de atividade)</td>
              <td>6</td>
              <td>{{$requerimento_docente->membro_comite_etica*6}}</td>  
            </tr>
            <tr>
              <td></td>
              <td class="h3">Score Total: </td>
              <td class="h3"> {{$requerimento_docente->doutorado*30 + $requerimento_docente->mestrado*15 +
                  $requerimento_docente->bolsista_produtividade*10 + $requerimento_docente->qualis_A1*10
                  + $requerimento_docente->qualis_A2*8 + $requerimento_docente->qualis_B1*6
                  + $requerimento_docente->qualis_B2*4 + $requerimento_docente->qualis_B3*3 +
                  $requerimento_docente->qualis_B4_B5*2 + $requerimento_docente->qualis_C*1
                  + $requerimento_docente->artigos_nao_qualis*0.5 + $requerimento_docente->eventos_internacionais*2
                  + $requerimento_docente->eventos_nacionais*1 + $requerimento_docente->patente_registrada*10
                  + $requerimento_docente->software_registrado*10 + $requerimento_docente->livro_com_conselho*10
                  + $requerimento_docente->livro_sem_conselho*5 + $requerimento_docente->livro_organizado*5
                  + $requerimento_docente->capitulo_de_livro*3 + $requerimento_docente->projeto_externo_coordenador*10
                  + $requerimento_docente->projeto_externo_colaborador*5 + $requerimento_docente->projeto_interno_coordenador*5
                  + $requerimento_docente->projeto_interno_colaborador*3 + $requerimento_docente->orientacao_doutorado*10
                  + $requerimento_docente->orientacao_mestrado*8 + $requerimento_docente->orientacao_iniciacao_cientifica*4
                  + $requerimento_docente->orientacao_TCC*2 + $requerimento_docente->orientacao_iniciacao_cientifica_junior*0.5
                  + $requerimento_docente->orientacao_monografia*2 + $requerimento_docente->co_orientacao_doutorado*6
                  + $requerimento_docente->bancas_de_defesa_doutorado*6 + $requerimento_docente->bancas_de_defesa_qualificacao_doutorado*3
                  + $requerimento_docente->bancas_de_defesa_mestrado*4 + $requerimento_docente->bancas_de_defesa_qualificacao_mestrado*2
                  + $requerimento_docente->bancas_de_defesa_TCC*1 + $requerimento_docente->membro_comite_etica*6 }}</td>
            </tr>
      </table><br>
    		<div class="text-center">
          <a href="{{action('PropController@deferirDocente', $requerimento_docente->id)}}" class="btn btn-success">Deferir</a>
          <a href="{{action('PropController@indeferirConfirmacao', $requerimento_docente->id)}}" class="btn btn-danger">Indeferir</a>
          <a href="{{url('voltarProp')}}" class="btn btn-secondary">Voltar</a>
    	  </div><br>
  	</form>
  </div>
</body>
</html>