<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="utf-8">
    <TITLE> Requerimentos de Passagem Docente</TITLE>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <style>
  table {
      font-family: Open Sans;
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #dddddd;
    }
  </style>

  </head>
  <body>
  	<header class="cabecalho">
      <h1 class="logo">
      <a title="UESPI - SIG Auxílios Acadêmicos"></a>
    </h1>
    <h1 class="titulo_site"> UESPI - SIG Auxílios Acadêmicos </h1>    
    </header>
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <div class="collapse navbar-collapse" id="nav-content">   
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class = "nav-link" href = "{{url('docentediaria')}}" ><font color=white>Solicitar Diária</font></a>
          </li>
          <li class="nav-item">
            <a class = "nav-link" href = "{{url('docentepassagem')}}" ><font color=white>Solicitar Passagem</font></a>
          </li>
          <li class="nav-item">
            <a class = "nav-link" href = "{{url('docentehistorico')}}" ><font color=white>Histórico de solicitações</font></a>
          </li>
        </ul>
        <form class="form-inline" action ="{{url('')}}" method="post">
          {!! csrf_field() !!}
            <button class="btn btn-outline-success" type="submit">Logout</button>
          </form>
        </div>
    </nav>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container">
      <br>
      <h1>Requerimento de Passagem</h1>
  	<form action="{{url('solicitacaopassagem')}}" method="post">
  		{!! csrf_field() !!}
      <br>
  	     <div>
  	        <label>Conta do Banco:</label>
  	        <input type="text" name = "conta">
  	        <label>Agência:</label>
  	        <input type="text" name = "agencia">
  	    </div><br>
  		<div>
  	        <label>Nome do Banco:</label>
  	        <input type="text" name = "banco">
  	    </div><br>
  	    <div>
  	        <label>Local do Evento:</label>
  	        <input type="text" name = "evento">
  	    </div><br>
  	    <div>
  	        <label>Período do Evento(Início/Termino):</label>
  	        <input name = "data_ida" type="date"/>
  	        <input name = "data_volta" type="date"/>
  	    </div><br>
  	    <div>
            <label>Abrangência do Evento:</label><br>
            <input type="radio" NAME="abrangencia_evento" VALUE="Regional"/><label>Regional</label>
            <input type="radio" NAME="abrangencia_evento" VALUE="Nacional"/><label>Nacional</label>
            <input type="radio" NAME="abrangencia_evento" VALUE="Internacional"/><label>Internacional</label>
        </div><br>
        <div>
            <label>Título do Trabalho:</label>
            <input type="text" name = "trabalho">
        </div><br>
        <div>
            <label>Recebeu auxílio nos anos anteriores?</label><br>
            <input type="radio" NAME="auxilio_anterior" VALUE="Sim"/><label>Sim</label>
            <input type="radio" NAME="auxilio_anterior" VALUE="Não"/><label>Não</label>
        </div><br>
  	    <div>
  	        <label>Ano:</label>
  	        <input type="text"  NAME="ano"/>
  	    </div><br>
  	     <div>
  	        <label>Descrição do auxílio:</label><br>
  			<textarea name="descricao"></textarea>
  	    </div><br>
  	    <div>
  	    <label>PDF do Trabalho:</label><br>
  	           <input name = "comprovante_trabalho" type="file" accept="application/pdf" >
  		</div><br>
  	    <div>
  			<label>Contra-Cheque:</label><br>
  	           <input type="file" accept="application/pdf" name = "contracheque">
  	    </div><br>
  	    <div>
  			<label>Currículo-Lattes:</label><br>
  	           <input type="file" accept="application/pdf" name = "curriculo">
  	    </div><br>
  	   
  	    <h2>Formulário de Produção</h2>

  		<table>
  			<tr>
  				<td><h3>Área de Avaliação:</h3></td>
  				<td></td>
          		<td></td>
  			</tr>
  			<tr>
  				<td>Discriminação</td>
  				<td>Escore por item</td>
  				<td>Quantidade</td>
  			</tr>
        		<tr>
          		<td>Doutorado</td>
          		<td>30</td>
          		<td><input type="number" name = "doutorado"></td>
        		</tr>
        		<tr>
          		<td>Mestrado</td>
          		<td>15</td>
          		<td><input type="number" name = "mestrado"></td>
        		</tr>
        		<tr>
          		<td>Bolsista de Produtividade em Pesquisa do CNPq</td>
          		<td>10</td>
          		<td><input type="number" name = "bolsista_produtividade"></td>
        		</tr>
        		<tr>
          		<td>Artigos publicados em periódicos científicos indexados:</td>
          		<td></td>
          		<td></td>
        		</tr>
        		<tr>
          		<td>Qualis A1</td>
          		<td>10</td>
          		<td><input type="number" name = "qualis_A1"></td>
        		</tr>
        		<tr>
          		<td>Qualis A2</td>
          		<td>8</td>
          		<td><input type="number" name = "qualis_A2"></td>
        		</tr>
        		<tr>
          		<td>Qualis B1</td>
          		<td>6</td>
          		<td><input type="number" name = "qualis_B1"></td>
        		</tr>
        		<tr>
          		<td>Qualis B2</td>
          		<td>4</td>
          		<td><input type="number" name = "qualis_B2"></td>
        		</tr>
        		<tr>
          		<td>Qualis B3</td>
          		<td>3</td>
          		<td><input type="number" name = "qualis_B3"></td>
        		</tr>
        		<tr>
          		<td>Qualis B4; B5</td>
          		<td>2</td>
          		<td><input type="number" name = "qualis_B4_B5"></td>
        		</tr>
        		<tr>
          		<td>Qualis C</td>
          		<td>1</td>
          		<td><input type="number" name = "qualis_C"></td>
        		</tr>
        		<tr>
          		<td>Artigos publicados em periódicos científicos NÃO 
          		classificados pelo sistema QUALIS</td>
          		<td>0.5</td>
          		<td><input type="number" name = "artigos_nao_qualis"></td>
        		</tr>
        		<tr>
          		<td>Trabalhos completos publicados em anais de congressos:</td>
          		<td></td>
          		<td></td>
        		</tr>
        		<tr>
          		<td>Eventos Internacionais</td>
          		<td>2</td>
          		<td><input type="number" name = "eventos_internacionais"></td>
        		</tr>
        		<tr>
          		<td>Eventos Nacionais e Regionais</td>
          		<td>1</td>
          		<td><input type="number" name = "eventos_nacionais"></td>
        		</tr>
        		<tr>
          		<td>Patente Registrada (carta patente)</td>
          		<td>10</td>
          		<td><input type="number" name = "patente_registrada"></td>
        		</tr>
        		<tr>
          		<td>Software Registrado</td>
          		<td>10</td>
          		<td><input type="number" name = "software_registrado"></td>
        		</tr>
        		<tr>
          		<td>Autor/Co-autor de livros publicados com ISBN:</td>
          		<td></td>
          		<td></td>
        		</tr>
        		<tr>
          		<td>Livro publicado por editora com conselho editorial, com no mínimo 60 páginas</td>
          		<td>10</td>
          		<td><input type="number" name = "livro_com_conselho"></td>
        		</tr>
        		<tr>
          		<td>Livro publicado sem conselho editorial, com no mínimo 60 páginas</td>
          		<td>5</td>
          		<td><input type="number" name = "livro_sem_conselho"></td>
        		</tr>
        		<tr>
          		<td>Livro Organizado</td>
          		<td>5</td>
          		<td><input type="number" name = "livro_organizado"></td>
        		</tr>
        		<tr>
          		<td>Capítulo de livro publicado por editora com conselho editorial</td>
          		<td>3</td>
          		<td><input type="number" name = "capitulo_de_livro"></td>
        		</tr>
        		<tr>
          		<td>Projeto de pesquisa com financiamento externo:</td>
          		<td></td>
          		<td></td>
        		</tr>
        		<tr>
          		<td>Como coordenador</td>
          		<td>10</td>
          		<td><input type="number" name = "projeto_externo_coordenador"></td>
        		</tr>
        		<tr>
          		<td>Como colaborador</td>
          		<td>5</td>
          		<td><input type="number" name = "projeto_externo_colaborador"></td>
        		</tr>
        		<tr>
          		<td>Projeto de pesquisa com financiamento interno(Editais PIP):</td>
          		<td></td>
          		<td></td>
        		</tr>
        		<tr>
          		<td>Como coordenador</td>
          		<td>5</td>
          		<td><input type="number" name = "projeto_interno_coordenador"></td>
        		</tr>
        		<tr>
          		<td>Como colaborador</td>
          		<td>3</td>
          		<td><input type="number" name = "projeto_interno_colaborador"></td>
        		</tr>
        		<tr>
          		<td>Orientação/Co-orientação(Concluída):</td>
          		<td></td>
          		<td></td>
        		</tr>
        		<tr>
          		<td>Orientação de Doutorado</td>
          		<td>10</td>
          		<td><input type="number" name = "orientacao_doutorado"></td>
        		</tr>
        		<tr>
          		<td>Orientação de Mestrado</td>
          		<td>8</td>
          		<td><input type="number" name = "orientacao_mestrado"></td>
        		</tr>
        		<tr>
          		<td>Orientação de Iniciação Científica</td>
          		<td>4</td>
          		<td><input type="number" name = "orientacao_iniciacao_cientifica"></td>
        		</tr>
        		<tr>
          		<td>Orientação de TCC de Graduação</td>
          		<td>2</td>
          		<td><input type="number" name = "orientacao_TCC"></td>
        		</tr>
        		<tr>
          		<td>Orientação de Iniciação Científica Júnior</td>
          		<td>0.5</td>
          		<td><input type="number" name = "orientacao_iniciacao_cientifica_junior"></td>
        		</tr>
        		<tr>
          		<td>Orientação de Monografia de Especialização</td>
          		<td>2</td>
          		<td><input type="number" name = "orientacao_monografia"></td>
        		</tr>
        		<tr>
          		<td>Co-orientação de Doutorado</td>
          		<td>6</td>
          		<td><input type="number" name = "co_orientacao_doutorado"></td>
        		</tr>
        		<tr>
          		<td>Participação em bancas de defesa:</td>
          		<td></td>
          		<td></td>
        		</tr>
        		<tr>
          		<td>Doutorado</td>
          		<td>6</td>
          		<td><input type="number" name = "bancas_de_defesa_doutorado"></td>
        		</tr>
        		<tr>
          		<td>Qualificação de Doutorado</td>
          		<td>3</td>
          		<td><input type="number" name = "bancas_de_defesa_qualificacao_doutorado"></td>
        		</tr>
        		<tr>
          		<td>Mestrado</td>
          		<td>4</td>
          		<td><input type="number" name = "bancas_de_defesa_mestrado"></td>
        		</tr>
        		<tr>
          		<td>Qualificação de Mestrado</td>
          		<td>2</td>
          		<td><input type="number" name = "bancas_de_defesa_qualificacao_mestrado"></td>
        		</tr>
        		<tr>
          		<td>TCC/Monografia</td>
          		<td>1</td>
          		<td><input type="number" name = "bancas_de_defesa_TCC"></td>
        		</tr>
        		<tr>
          		<td>Membro do Comitê de Ética em Pesquisa da UESPI (por ano de atividade)</td>
          		<td>6</td>
          		<td><input type="number" name = "membro_comite_etica"></td>
        		</tr>
  		</table><br>       
      <div class="text-center">
          <button class="btn btn-success" type="submit">Solicitar</button>
          <a href="{{url('voltarDocente')}}" class="btn btn-danger">Cancelar</a>
      </div><br>
      <input type="hidden" name = "tipo" value = "passagem">
        <input type="hidden" name = "status" value = "PROP">
        <input type="hidden" name = "matricula" value = "<?php isset($_SESSION['matricula']) ? print $_SESSION['matricula'] : false; ?>">
        <input type="hidden" name = "nome" value = "<?php isset($_SESSION['nome']) ? print $_SESSION['nome'] : false; ?>">
    </form> 
  </div>
</body>
</html>