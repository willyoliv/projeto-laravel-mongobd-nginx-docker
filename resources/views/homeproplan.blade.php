<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home PROPLAN</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
  </head>

  <body>
  	<header class="cabecalho">
  		<h1 class="logo">
			<a title="UESPI - SIG Auxílios Acadêmicos"></a>
		</h1>
		<h1 class="titulo_site"> UESPI - SIG Auxílios Acadêmicos </h1>	 	
  	</header>
  		<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
		  <div class="collapse navbar-collapse" id="nav-content">   
			  <ul class="navbar-nav mr-auto">
			    <li class="nav-item">
			      <a class = "nav-link" href = "{{url('proplansolicitacoes')}}" ><font color=white>Solicitações de Auxílios</font></a>
			    </li>
			  </ul>
			  <form class="form-inline" action ="{{url('')}}" method="post">
			  	<!--<label>@Fulano Silva / 101010</label>-->
			  	{!! csrf_field() !!}
	      		<button class="btn btn-outline-success" type="submit">Logout</button>
	    	  </form>
    	  </div>
		</nav>
  </body>
