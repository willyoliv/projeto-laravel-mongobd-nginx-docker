<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <TITLE>Histórico Requerimentos Docente</TITLE>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
  	<style>
	table {
		  font-family: Open Sans;
		  border-collapse: collapse;
		  width: 100%;
		}

		td, th {
		  border: 1px solid #dddddd;
		  text-align: left;
		  padding: 8px;
		}

		tr:nth-child(even) {
		  background-color: #dddddd;
		}
	</style>
  </head>
  <body>
  	<header class="cabecalho">
  		<h1 class="logo">
			<a title="UESPI - SIG Auxílios Acadêmicos"></a>
		</h1>
		<h1 class="titulo_site"> UESPI - SIG Auxílios Acadêmicos </h1>	 	
  	</header>
  		<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
		  <div class="collapse navbar-collapse" id="nav-content">   
			  <ul class="navbar-nav mr-auto">
			    <li class="nav-item">
			      <a class = "nav-link" href = "{{url('docentediaria')}}" ><font color=white>Solicitar Diária</font></a>
			    </li>
			    <li class="nav-item">
			      <a class = "nav-link" href = "{{url('docentepassagem')}}" ><font color=white>Solicitar Passagem</font></a>
			    </li>
			    <li class="nav-item">
			      <a class = "nav-link" href = "{{url('docentehistorico')}}" ><font color=white>Histórico de solicitações</font></a>
			    </li>
			  </ul>
			  <form class="form-inline" action ="{{url('')}}" method="post">
			  	<!--<label>@Fulano Silva / 101010</label>-->
			  	{!! csrf_field() !!}
	      		<button class="btn btn-outline-success" type="submit">Logout</button>
	    	  </form>
    	  </div>
		</nav>
		<div class="container">
	    <table>
	      	<h3>Requerimentos Indeferidos</h3>
	      	<tr>
				<td>Tipo do requerimento</td>
				<td>Evento</td>
				<td>Justificativa</td>
			</tr>
	      	@foreach($requerimento_indeferidos as $requerimento)
	      		<tr>
	        		<td>{{$requerimento->tipo}}</td>
	        		<td>{{$requerimento->evento}}</td>
	        		<td><a href="#editEmployeeModal" class="btn btn-info" data-toggle="modal">Ver</td>
					<div id="editEmployeeModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<form>
								<div class="modal-header">						
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">					
									<div class="form-group">
										<label>INDEFERIMENTO</label>
										<br>
										<label>Justificativa: {{$requerimento->observacao}} </label>
									</div>					
								</div>
								<div class="modal-footer">
									<input type="button" class="btn btn-default" data-dismiss="modal" value="OK">
								</div>
							</form>
						</div>
					</div>
				</div>
	        	</tr>
	      	@endforeach
			</table><br>
			<table>
	      		<h3>Requerimentos para Prestação de Contas</h3>
	      		<tr>
					<td>Tipo do requerimento</td>
					<td>Evento</td>
					<td>Prestação de Conta</td>
				</tr>
	      		@foreach($requerimento_prestacao as $requerimento)
	      		<tr>
	        		<td>{{$requerimento->tipo}}</td>
	        		<td>{{$requerimento->evento}}</td>
	        		<td><a href="{{action('RequerimentoDocController@alterarStatus', $requerimento->id)}}" class="btn btn-info">Ver Detalhes</a></td>		
	        	</tr>
	      		@endforeach
			</table><br>
			<table>
	      		<h3>Requerimentos em Andamento</h3>
	      		<tr>
					<td>Tipo do requerimento</td>
					<td>Evento</td>
					<td>Setor Atual</td>
				</tr>
	      		@foreach($requerimento_andamento as $requerimento)
	      		<tr>
	        		<td>{{$requerimento->tipo}}</td>
	        		<td>{{$requerimento->evento}}</td>
	        		<td>{{$requerimento->status}}</td>	
	        	</tr>
	      		@endforeach
			</table>
		<br>
		<table>
	      		<h3>Requerimentos Finalizados</h3>
	      		<tr>
					<td>Tipo do requerimento</td>
					<td>Evento</td>
	      		@foreach($requerimento_finalizados as $requerimento)
	      		<tr>
	        		<td>{{$requerimento->tipo}}</td>
	        		<td>{{$requerimento->evento}}</td>	
	        	</tr>
	      		@endforeach
			</table>
		<br>
		<form action="{{url('homealuno')}}" method="get">
			<div class="text-center">
				<button type="submit" class="btn btn-secondary" >Voltar</button>
			</div><br>
		</form>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>