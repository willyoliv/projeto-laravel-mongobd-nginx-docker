<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Indeferir Formulário</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
  <style>
	table {
		  font-family: Open Sans;
		  border-collapse: collapse;
		  width: 100%;
		}

   h1{
      padding-top: 10px;
      padding-bottom: 10px;
      font-family: Open Sans;
   }
   button#cancelar{
      margin-left: 20px; 
   }
   textarea{
      width: 300px;
      height: 150px;
   }
	</style>
  </head>
  <body class = "text-center">
    <header class="cabecalho">
      <h1 class="logo">
      <a title="UESPI - SIG Auxílios Acadêmicos"></a>
    </h1>
    <h1 class="titulo_site"> UESPI - SIG Auxílios Acadêmicos </h1>    
    </header>
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <div class="collapse navbar-collapse" id="nav-content">   
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class = "nav-link" href = "{{url('solicitacoes')}}" ><font color=white>Solicitações de Auxílios</font></a>
          </li>
        </ul>
        <form class="form-inline" action ="{{url('')}}" method="post">
          <!--<label>@Fulano Silva / 101010</label>-->
          {!! csrf_field() !!}
          <button class="btn btn-outline-success" type="submit">Logout</button>
        </form>
        </div>
    </nav><br>
      	<meta charset="UTF-8">
      	<h1>Indeferir Solicitação</h1>
      <div class="container">
      	<form action="{{action('PropController@indeferir', $id)}}" method="post">
      		{!! csrf_field() !!}
          <div>
              <label>Justificativa:</label><br>
              <textarea type="msg" name = "observacao" placeholder="Escreva aqui a justificativa..."></textarea>
          </div><br>
          <div>
              <button class="btn btn-success" id="confirmar" type="submit">Confirmar</button>
              <form action="{{url('homeprop')}}" method="get">
                  <button type="submit" class="btn btn-danger">Cancelar</button>
              </form>
          </div><br>
      	</form>
    </div>
</body>
</html>