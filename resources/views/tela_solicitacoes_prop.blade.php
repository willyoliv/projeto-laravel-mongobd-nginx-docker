<!DOCTYPE html>
<html>
  <head>
  	<head>
    <meta charset="utf-8">
    <title>Solicitações PROP</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <style>
  	table {
  		  font-family: arial, sans-serif;
  		  border-collapse: collapse;
  		  width: 100%;
  		}

  		td, th {
  		  border: 1px solid #dddddd;
  		  text-align: left;
  		  padding: 8px;
  		}

  		tr:nth-child(even) {
  		  background-color: #dddddd;
  	  }
  	</style>
  </head>
  <body>
    <header class="cabecalho">
      <h1 class="logo">
      <a title="UESPI - SIG Auxílios Acadêmicos"></a>
    </h1>
    <h1 class="titulo_site"> UESPI - SIG Auxílios Acadêmicos </h1>    
    </header>
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <div class="collapse navbar-collapse" id="nav-content">   
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class = "nav-link" href = "{{url('solicitacoes')}}" ><font color=white>Solicitações de Auxílios</font></a>
          </li>
        </ul>
        <form class="form-inline" action ="{{url('')}}" method="post">
          <!--<label>@Fulano Silva / 101010</label>-->
          {!! csrf_field() !!}
            <button class="btn btn-outline-success" type="submit">Logout</button>
          </form>
        </div>
    </nav><br>
  </head>
  <body>
  	<meta charset="UTF-8">
  <div class="container">
    <h1>Solicitações Docente</h1>
  	<form action="/" method="post">
  		<table>
  			<tr>
  				<td>Requerente</td>
  				<td>Tipo</td>
  				<td>Data do evento</td>
          <td>Data da solicitação</td>
  				<td>Parecer</td>
  			</tr> 
  			@foreach($requerimentos_docente as $requerimento)
        		<tr>
               	<td>{{$requerimento->nome}}</td>
        			<td>{{$requerimento->tipo}}</td>
          		<td>{{date( 'd/m/Y' , strtotime($requerimento->data_ida))}}</td>
              <td>{{date( 'd/m/Y' , strtotime($requerimento->created_at))}}</td>
          		<td><a href="{{action('PropController@visualizar', $requerimento->id)}}" class="btn btn-info">Ver Detalhes</a></td>
        		@endforeach
        		</tr>
  		</table>
  		<h1>Solicitações Técnico</h1>
  		<table>
  			<tr>
  				<td>Requerente</td>
  				<td>Tipo</td>
  				<td>Data do evento</td>
          <td>Data da solicitação</td>
  				<td>Parecer</td>
  			</tr> 
  			@foreach($requerimentos_tecnico as $requerimento)
        		<tr>
               	<td>{{$requerimento->nome}}</td>
        			<td>{{$requerimento->tipo}}</td>
          		<td>{{date( 'd/m/Y' , strtotime($requerimento->data_ida))}}</td>
              <td>{{date( 'd/m/Y' , strtotime($requerimento->created_at))}}</td>
          		<td><a href="{{action('PropController@visualizarTecnico', $requerimento->id)}}" class="btn btn-info">Ver Detalhes</a></td>
        		@endforeach
        		</tr>
  		</table>
  		<h1>Solicitações Aluno</h1>
  		<table>
  			<tr>
  				<td>Requerente</td>
  				<td>Tipo</td>
  				<td>Data do evento</td>
          <td>Data da solicitação</td>
  				<td>Parecer</td>
  			</tr> 
  			@foreach($requerimentos_aluno as $requerimento)
        		<tr>
               	<td>{{$requerimento->nome}}</td>
        			<td>{{$requerimento->tipo}}</td>
          		<td>{{date( 'd/m/Y' , strtotime($requerimento->data_ida))}}</td>
              <td>{{date( 'd/m/Y' , strtotime($requerimento->created_at))}}</td>
          		<td><a href="{{action('PropController@visualizarAluno', $requerimento->id)}}" class="btn btn-info">Ver Detalhes</a></td>
              
        		@endforeach
        		</tr>
  		</table>
      <br><br>
  	</form>
  </div>
</body>
</html>