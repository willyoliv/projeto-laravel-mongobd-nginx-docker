<!-- teste.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
  </head>
  <body>
    <table>
    <thead>
      <tr>
        <th>ID</th>
        <th>Matricula</th>
        <th>Senha</th>
        <th>Cargo</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($usuarios as $usuario)
      <tr>
        <td>{{$usuario->id}}</td>
        <td>{{$usuario->matricula}}</td>
        <td>{{$usuario->senha}}</td>
        <td>{{$usuario->cargo}}</td>
        <td>{{$usuario->nome}}</td>

      @endforeach
  </body>
</html>