<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="utf-8">
    <TITLE>Histórico Requerimentos Técnico</TITLE>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
  	<style>
	table {
		  font-family: Open Sans;
		  border-collapse: collapse;
		  width: 100%;
		}

		td, th {
		  border: 1px solid #dddddd;
		  text-align: left;
		  padding: 8px;
		}

		tr:nth-child(even) {
		  background-color: #dddddd;
		}
	</style>
  </head>
  <body>
  	<header class="cabecalho">
  		<h1 class="logo">
			<a title="UESPI - SIG Auxílios Acadêmicos"></a>
		</h1>
		<h1 class="titulo_site"> UESPI - SIG Auxílios Acadêmicos </h1>	 	
  	</header>
  		<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
		  <div class="collapse navbar-collapse" id="nav-content">   
			  <ul class="navbar-nav mr-auto">
			    <li class="nav-item">
			      <a class = "nav-link" href = "{{url('tecnicodiaria')}}" ><font color=white>Solicitar Diária</font></a>
			    </li>
			    <li class="nav-item">
			      <a class = "nav-link" href = "{{url('tecnicopassagem')}}" ><font color=white>Solicitar Passagem</font></a>
			    </li>
			    <li class="nav-item">
			      <a class = "nav-link" href = "{{url('tecnicohistorico')}}" ><font color=white>Histórico de solicitações</font></a>
			    </li>
			  </ul>
			  <form class="form-inline" action ="{{url('')}}" method="post">
			  	{!! csrf_field() !!}
	      		<button class="btn btn-outline-success" type="submit">Logout</button>
	    	  </form>
    	  </div>
		</nav>
		<br>
		<div class="container">
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
		</div>
	<div class="container">
		<br>
		<h1>Requerimento de Passagem</h1>
		<form action="{{url('solicitacaopassagemtecnico')}}" method="post">
			{!! csrf_field() !!}
			<br>
		    <div>
		        <label>Conta do Banco:</label>
		        <input type="text" name = "conta">
		        <label>Agência:</label>
		        <input type="text" name = "agencia">
		    </div><br>
			<div>
		        <label>Nome do Banco:</label>
		        <input type="text" name = "banco">
		    </div><br>
		    <div>
		        <label>Local do Evento:</label>
		        <input type="text" name = "evento">
		    </div><br>
		    <div>
		        <label>Período do Evento(Início/Termino):</label>
		        <input name = "data_ida" type="date"/>
		        <input name = "data_volta" type="date"/>
		    </div><br>
		    <div>
		        <label>Abrangência do Evento:</label><br>
		        <input type="radio" NAME="abrangencia_evento" VALUE="Regional"/><label>Regional</label>
		        <input type="radio" NAME="abrangencia_evento" VALUE="Nacional"/><label>Nacional</label>
		        <input type="radio" NAME="abrangencia_evento" VALUE="Internacional"/><label>Internacional</label>
		    </div><br>
		    <div>
		        <label>Título do Trabalho:</label>
		        <input type="text" name = "trabalho">
		    </div><br>
		    <div>
		        <label>Recebeu auxílio nos anos anteriores?</label><br>
		        <input type="radio" NAME="auxilio_anterior" VALUE="Sim"/><label>Sim</label>
		        <input type="radio" NAME="auxilio_anterior" VALUE="Não"/><label>Não</label>
		    </div><br>
		    <div>
		        <label>Ano:</label>
		        <input type="text" name = "ano"/>
		    </div><br>
		     <div>
		        <label>Descrição do auxílio:</label><br>
				<textarea name="descricao"></textarea>
		    </div><br>
		    <div>
		    <label>Contracheque:</label><br>
		           <input name = "contracheque" type="file" accept="application/pdf" >
			</div><br>  
		    <div>
		    <label>PDF do Trabalho:</label><br>
		           <input name = "comprovante_trabalho" type="file" accept="application/pdf" >
			</div><br>     
      		<div class="text-center">
          		<button class="btn btn-success" type="submit">Solicitar</button>
         		<a href="{{url('voltarTecnico')}}" class="btn btn-danger">Cancelar</a>
      		</div><br>
      		<input type="hidden" name = "tipo" value = "passagem">
        <input type="hidden" name = "status" value = "PROP">
        <input type="hidden" name = "matricula" value = "<?php isset($_SESSION['matricula']) ? print $_SESSION['matricula'] : false; ?>">
        <input type="hidden" name = "nome" value = "<?php isset($_SESSION['nome']) ? print $_SESSION['nome'] : false; ?>">
    </form> 
  </div>
</body>
</html>