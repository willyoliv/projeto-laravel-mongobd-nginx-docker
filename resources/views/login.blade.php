<!DOCTYPE html>
<html lang='pt-br'>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Login</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <style>
    /*Login*/
    .container{
      padding-top: 10%;
      max-width: 250px;
      margin-top: 0px;
    }
    .container h1{
      padding-top: 10px;
      padding-bottom: 10px;
      color: #000000;
      font-family: Open Sans;
      font-size: 1.5em;
    }
    a{
      float: right;
    }
    input{
      margin-bottom: 10px;
    }
    /*Cabeçalho*/
    .cabecalho_login {
      width: 100%;
      height: 150px; 
      float: left;
      margin-top: -18px;
      padding: 20px 4%; 
      background-color: #3db0f7;
    }
    .titulo_site_login {
      float: left;
      margin-top: 30px;
      margin-left: 30px;
      font-weight: bold;
      font-family: Open Sans;
      color: #ffffff;  
    }
    </style>

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <!--<h1 class="text-primary">UESPI - SIG Diárias e Passagens Aéreas<h1>-->
    <header class="cabecalho_login">
      <h1 class="logo">
        <a title="UESPI - SIG Auxílios Acadêmicos"></a>
      </h1>
      <h1 class="titulo_site_login"> UESPI - SIG Auxílios Acadêmicos </h1>      
    </header>
    <hr>
    <div class="container">
      <form role="form" action="{{url('home')}}" method="post" >
          {!! csrf_field() !!}
          <h1 class="h3 mb-3 font-weight-normal">Login do Usuário</h1>
          <div class="form-group ls-login-user">
            <input class="form-control ls-login-bg-user input-lg" id="userLogin" type="text" aria-label="Usuário" name = "matricula" placeholder="Matrícula" required = "true">
          </div>
          <div class="form-group ls-login-password">
            <input class="form-control ls-login-bg-password input-lg" id="userPassword" type="password" aria-label="Senha" name = "senha" placeholder="Senha" required = "true">
          </div>
          <input type="submit" value="Entrar" class="btn btn-primary btn-lg btn-block">
      </form>
    </div>
  </body>
</html>