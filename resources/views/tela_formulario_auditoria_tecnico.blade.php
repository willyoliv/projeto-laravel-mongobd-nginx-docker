<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Formulário Auditoria</title>
    <link rel="stylesheet" href="{{ asset('css/styles.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <style>
      /* RESET */
      * {margin: 0; padding: 0; font-size: 100%; font-family: 'Open Sans', sans-serif; font-weight: normal;
      box-sizing: border-box;}
      img {
        max-width: : 100%
      }
      ul {
        list-style: none;
      }
      a {
        text-decoration: none;
      }
      h2 {
        font-size: 1.5em; 
        color: #333;
      }
      p {
        font-size: 1em; 
        color: #777;
      }
      h1 {
        color: #ffffff;
      }
      h3 {
        font-size: 1.5em; 
      }

      /* CABEÇALHO */
      .cabecalho {
        width: 100%; 
        float: left; 
        padding: 15px 4%; 
        background-color: #3db0f7;
      }
      .cabecalho form {
        width: 30%; 
        float: right;
      }
      .cabecalho button {
        width: 15%; 
        float: right; 
        padding: 15px 10px; 
        background-color: #0cae98; 
        color: #fff;
        cursor: pointer;
      }
      .logo a {
        width: 71px; 
        height: 119px;
        float: left; 
        background: url(../img/logo1.png) no-repeat;
        margin-left: 0px;
        margin-right: 30px;
      }
      .titulo_site {
        float: left;
        margin-top: 30px;
        margin-left: 30px;
        font-weight: bold;
        color: #ffffff;  
      }
      .form-inline label{
        color: #ffffff;
        margin-right: 10px; 
      }
      .nav-link{
        margin-left: 25px;
      }
      table {
        font-family: 'Open Sans', sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
      }

      tr:nth-child(even) {
        background-color: #dddddd;
      }
      button#indeferir{
        margin-left: 10px;
      }
    </style>
  </head>
  <body>
    <header class="cabecalho">
      <h1 class="logo">
      <a title="UESPI - SIG Auxílios Acadêmicos"></a>
    </h1>
    <h1 class="titulo_site"> UESPI - SIG Auxílios Acadêmicos </h1>    
    </header>
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <div class="collapse navbar-collapse" id="nav-content">   
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class = "nav-link" href = "{{url('auditoriasolicitacoes')}}" ><font color=white>Solicitações de Auxílios</font></a>
          </li>
        </ul>
        <form class="form-inline" action ="{{url('')}}" method="post">
          {!! csrf_field() !!}
            <button class="btn btn-outline-success" type="submit">Logout</button>
          </form>
        </div>
    </nav><br>
    <div class="container">
    	<form method="post">
    		{!! csrf_field() !!}
        <table>
          <tr>
            <h3>Dados do Requerente:</h3>
          </tr>
          <tr>
            <td>Requerente:</td>
            <td>{{$requerimento_tecnico->nome}}</td>
          </tr>
          <tr>
            <td>Matrícula:</td>
            <td>{{$requerimento_tecnico->matricula}}</td>
          </tr>
          <tr>
            <td>Conta do Banco:</td>
            <td>{{$requerimento_tecnico->conta}}</td>
          </tr>
           <tr>
            <td>Agência:</td>
            <td>{{$requerimento_tecnico->agencia}}</td>
          </tr>
          <tr>
            <td>Nome do Banco:</td>
            <td>{{$requerimento_tecnico->banco}}</td>
          </tr>
          <tr>
            <td>Local do Evento:</td>
            <td>{{$requerimento_tecnico->evento}}</td>
          </tr>
          <tr>
            <td>Período do Evento(Ínicio/Término):</td>
            <td>{{ date( 'd/m/Y' , strtotime($requerimento_tecnico->data_ida))}} \ {{ date( 'd/m/Y' , strtotime($requerimento_tecnico->data_volta))}}</td>
          </tr>
          <tr>
            <td>Abrangência do Evento:</td>
            <td>{{$requerimento_tecnico->abrangencia_evento}}</td>
          </tr>
          <tr>
            <td>Título do Trabalho:</td>
            <td>{{$requerimento_tecnico->trabalho}}</td>
          </tr>
          <tr>
            <td>Recebeu auxílio nos anos anteriores?</td>
            <td>{{$requerimento_tecnico->auxilio_anterior}}</td>
          </tr>
          <tr>
            <td>Ano:</td>
            <td>{{$requerimento_tecnico->ano}}</td>
          </tr>
          <tr>
            <td>Descrição do Auxílio:</td>
            <td>{{$requerimento_tecnico->descricao}}</td>
          </tr>
          <tr>
            <td>Comprovante:</td>
            <td>{{$requerimento_tecnico->comprovante_trabalho}}</td>
          </tr>
          <tr>
            <td>Contracheque:</td>
            <td>{{$requerimento_tecnico->contracheque}}</td>
          </tr>
          <tr>
            <td>PRESTAÇÃO DE CONTAS</td>
            <td>{{$requerimento_tecnico->comprovante_evento}}</td>
          </tr>
        </table><br>
        <div class="text-center">
          <a href="{{action('AuditoriaController@deferirTecnico', $requerimento_tecnico->id)}}" class="btn btn-success">Deferir</a>
          <a href="{{action('AuditoriaController@indeferirConfirmacao', $requerimento_tecnico->id)}}" class="btn btn-danger">Indeferir</a>
          <a href="{{url('voltarAuditoria')}}" class="btn btn-secondary">Voltar</a>
        </div><br>
        </div><br>
    </form>
  </div>
</body>
</html>